angular.module("quantumTestApp").factory "appEvents", ->
	STORE_UPDATED: 'store:updated'
	CACHE_UPDATED: 'cache:updated'
	ADD_TO_CACHE: 'cache:add'
	CREATE_ITEM: 'cache:create'
	DELETE_ITEM: 'cache:delete'
	UPDATE_CACHE_ITEM: 'cache:update'
	STORE_COMMIT: 'store:commit'