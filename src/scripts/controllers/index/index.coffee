angular.module("quantumTestApp").controller "IndexController", (
	$rootScope
	$scope
	appEvents
	Store
	Cache
) ->

	$scope.data = {}
	$scope.dataCache = {}
	$scope.selectedItem = null
	$scope.selectedCacheItem = null

	unselect =->
		$scope.selectedItem = null
		$scope.selectedCacheItem = null

	$scope.reset =->
		Store.reset()
		Cache.reset()

	$scope.commit =->
		updatedCacheItems = Store.update Cache.getData()
		Cache.update updatedCacheItems
#		Cache.reset()
		unselect()

	$scope.addToCache =->
		unless $scope.selectedItem
			return

		$rootScope.$emit appEvents.ADD_TO_CACHE, $scope.selectedItem
		unselect()

	$scope.delete =->
		unless $scope.selectedCacheItem
			return

		$rootScope.$emit appEvents.DELETE_ITEM, $scope.selectedCacheItem.id
		unselect()

	$scope.create =->
		unless $scope.selectedCacheItem
			return

		item = new StoreItem()

		item.setParent $scope.selectedCacheItem.id
		item.setValue prompt "Enter label", ""

		$rootScope.$emit appEvents.CREATE_ITEM, item
		unselect()

	$scope.selectCacheItem = (itemId)->
		$scope.selectedCacheItem = Cache.getItemById(itemId)

	$scope.selectItem = (itemId)->
		#$scope.selectedItem = Store.getItemById itemId
		$scope.selectedItem = Store.getItemById(itemId).clone() #to avoid update original tree by link

	$scope.edit =->
		unless $scope.selectedCacheItem
			return

		dialog = prompt "Enter new label", $scope.selectedCacheItem.value

		if dialog
			$scope.selectedCacheItem.setValue dialog

		$rootScope.$emit appEvents.UPDATE_CACHE_ITEM, $scope.selectedCacheItem
		unselect()

	$rootScope.$on appEvents.STORE_UPDATED, (e, data)->
		$scope.data = data
		unselect()


