angular.module("quantumTestApp").config (
	$locationProvider,
	StoreDataLoaderProvider
) ->

	$locationProvider.html5Mode(false)

	StoreDataLoaderProvider.url = "./store_items.json"
	StoreDataLoaderProvider.timeout = 600
	StoreDataLoaderProvider.DEBUG = true