
parentIdCache = [0]
childIdCache = []
nodeCache = {}
alphabet = "abcdef"
numbers = "1234567890"
currentItem = 0

tree =
	0:
		id: 0
		parentId: -1
		deleted: false
		value: "ROOT"

getRandomItem = (list) ->
	list[Math.floor(Math.random() * list.length)]

getValue =->
	(alphabet[Math.floor(Math.random() * alphabet.length)] for a in [0..10]).join('')

getId =->
	string = numbers + alphabet
	(string[Math.floor(Math.random() * string.length)] for a in [0..16]).join('')


createNode = (parentId)->
	currentItem = currentItem + 1

	id: getId()
	parentId: parentId
	deleted: false
	value: currentItem + "_" + getValue()

addChild = (parentId)->

	if childIdCache.length is 0
		return

	unless parentId of tree
		tree[parentId] = nodeCache[parentId]

	childId = getRandomItem childIdCache

	unless childId of tree
		tree[childId] = nodeCache[childId]

	tree[childId].parentId = parentId

	index = childIdCache.indexOf childId
	childIdCache.splice index, 1

	r1 = Math.random();
	r2 = Math.random();

	if r1 > 0.1
		if r2 > 0.4
			addChild(parentId)
		else
			addChild(childId)
	else
		addChild(0)

count = 50

for i in [0..count]
	node = createNode 0
	nodeCache[node.id] = node
	parentIdCache.push node.id
	childIdCache.push node.id

addChild(0)

console.log JSON.stringify tree, null, "\t"

