angular.module("quantumTestApp").directive 'treeView', (
	$rootScope
)->
		restrict: 'A'
		scope:
			updateEvent: "@"
			onSelect: "="

		link: (scope, element)->
			_data = {}
			_drawn = []
			_childrenCache = {}
			lastSelectedView = null

			onClick = (item)->
				itemView = angular.element this
				scope.onSelect(item.id)

				if lastSelectedView
					lastSelectedView.css
						color: "#000"
						fontWeight: "normal"

				itemView.css
					color: "green"
					fontWeight: "bold"

				lastSelectedView = itemView

			createItem = (item, indent) ->
				label = ''
				cls = ''

				if item.deleted
					label += "(X) "
					cls += "removed"

				itemView = angular.element "<div class=\"item pointer #{cls}\" id=\"#{item.id}\">#{indent}&#9474;#{label}#{item.value}</div>"

				unless item.deleted
					itemView.on 'click', onClick.bind(itemView, item)

				return itemView

			drawItem = (item, indent = "")->
				_drawn.push item.id

				itemView = createItem item, indent
				element.append itemView

				indent = indent + "&emsp;"

#				_.each item.children, (itemId)->
				_.each _childrenCache[item.id], (itemId)->
					if itemId of _data
						drawItem _data[itemId], indent

			prepareChildrenTree = (rawData)->
				children = {}

				_.each rawData, (item)->
					unless item.parentId of children
						children[item.parentId] = []

					if item.parentId of rawData
						unless item.id in children[item.parentId]
							children[item.parentId].push item.id

				return children

			findRoot = (item)->
				if item.parentId of _data
					return findRoot(_data[item.parentId])
				else
					return item

			update = (e, rawData)->
				_data = rawData
				_childrenCache = prepareChildrenTree _data

				_drawn = []
				element.empty()

				_.each _data, (item) ->
					unless item.id in _drawn
						drawItem findRoot _data[item.id]

			$rootScope.$on scope.updateEvent, update

			return
