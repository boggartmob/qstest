angular.module("quantumTestApp").provider "StoreDataLoader", ->
	_cache = null
	@url = "/items.json"
	@DEBUG = false

	@$get = (
		$q
		$http
	)=>
		loadItems: (force = false)=>
			deferred = $q.defer();

			if @DEBUG
				console.log "StoreDataLoader DEBUG"
				_cache = __TEST_DATA__
				force = false

			if _cache and !force
				deferred.resolve _cache
			else
				$http.get(@url).success (data)=>
					_cache = data
					deferred.resolve _cache
				.error (e)->
					deferred.reject("Error message")

			return deferred.promise

	return