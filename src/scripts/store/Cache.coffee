angular.module("quantumTestApp").factory "Cache", (
	appEvents
	$rootScope
) ->

	self = {}
	_data = {}
	_childrenCache = new ChildrenCache()

	_updated =->
		$rootScope.$emit appEvents.CACHE_UPDATED, _data

	self.reset =->
		_data = {}
		_childrenCache.reset()
		_updated()

	self.getData =->
		return _data

	self.getItemById = (id)->
		return _data[id]

	self.update = (items)->
		_.each items, (item)->
			_data[item.id] = item

		_updated()

	$rootScope.$on appEvents.DELETE_ITEM, (e, id)->
		_.each (_childrenCache.getSubtreeIds id), (subtreeItemId)->
			_data[subtreeItemId].remove()

		_updated()

	$rootScope.$on appEvents.UPDATE_CACHE_ITEM, (e, item)->
		_data[item.id] = item
		_updated()

	$rootScope.$on appEvents.CREATE_ITEM, (e, item)->
		_data[item.id] = item
		_childrenCache.update item

		_updated()

	$rootScope.$on appEvents.ADD_TO_CACHE, (e, item)->
		if item.id of _data
			return

		_data[item.id] = item
		_childrenCache.update item

		if item.parentId of _data and _data[item.parentId].deleted
			_.each (_childrenCache.getSubtreeIds item.parentId), (subtreeItemId)->
				_data[subtreeItemId].remove()

		_updated()

	return self
