angular.module("quantumTestApp").factory "Store", (
	$rootScope
	appEvents
	StoreDataLoader
) ->

	self = {}
	_data = {}
	_childrenCache = new ChildrenCache()

	init = ->
		_data = {}
		_childrenCache.reset()

		StoreDataLoader.loadItems().then (data)->
			_.each data, (item)->
				model = new StoreItem(item)
				_data[item["id"]] = model

				_childrenCache.update model

			$rootScope.$emit appEvents.STORE_UPDATED, _data

	init()

	self.reset =->
		init()

	self.update = (items)->
		deletedItems = []
		updatedCacheItems = {}

		_.each items, (_item)->
			item = _item.clone() #to avoid update cache tree by link

			_childrenCache.update item

			if item.deleted
				deletedItems.push item

			_data[item.id] = item

		_.each deletedItems, (item)->
			_.each (_childrenCache.getSubtreeIds item.id), (subtreeItemId)->
				_data[subtreeItemId].remove()

		#Get the changes made during the subtree removal.
		#In case of traffic economy it can be just removed items IDs.
		_.each items, (item)->
			updatedCacheItems[item.id] = _data[item.id].clone()

		$rootScope.$emit appEvents.STORE_UPDATED, _data

		return updatedCacheItems

	self.getData = ->
		return _data

	self.getItemById = (id)->
		return _data[id]

	return self