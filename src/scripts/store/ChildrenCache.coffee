class ChildrenCache

	constructor: (items = [])->
		@_childrenCache = {}

		_.each items, (item)->
			update item

	reset: ()->
		@_childrenCache = {}

	update: (item)->
		unless item.parentId of @_childrenCache
			@_childrenCache[item.parentId] = []

		unless item.id in @_childrenCache[item.parentId]
			@_childrenCache[item.parentId].push item.id

	getSubtreeIds: (id, acc = [])->
		acc.push id

		if id of @_childrenCache
			_.each @_childrenCache[id], (itemId)=>
				acc.push itemId
				return @getSubtreeIds itemId, acc

		return acc

	getChildren: (id)->
		return @_childrenCache[id]

