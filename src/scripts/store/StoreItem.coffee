class StoreItem

	getStructure =->
		id: Math.random().toString()
		parentId: -1
		deleted: false
		value: ""

	constructor: (_rawData = null)->
		unless _rawData
			_rawData = getStructure()

		_.extend @, _rawData

	clone: ->
		item = getStructure()
		item.id = @id
		item.parentId = @parentId
		item.deleted = @deleted
		item.value = @value

		return new StoreItem(item)

	setParent: (newValue)->
		unless @deleted
			@parentId = newValue

	setValue: (newValue)->
		unless @deleted
			@value = newValue

	remove: ->
		@deleted = true
