module.exports = (grunt) ->
	require("load-grunt-tasks") grunt

	grunt.initConfig
		config:
			src: require("./bower.json").srcPath or "src"
			dist: "dist"
			tmp: ".tmp"

		clean:
			dist:
				files: [
					dot: true
					src: [
						".tmp"
						"<%= config.dist %>/*"
						"!<%= config.dist %>/.git*"
					]
				]

		useminPrepare:
			html: [
				"<%= config.dist %>/index.html"
			]
			options:
				dest: ".tmp"
				flow:
					html:
						steps:
							js: [
								"concat"
								"uglifyjs"
							]
							css: [
								"concat"
								"cssmin"
							]

						post: {}

		coffee:
			options:
				sourceMap: false
				sourceRoot: ''
				bare: true
			dist:
				files: [{
					expand: true
					cwd: '<%= config.src %>/scripts'
					src: '**/*.coffee'
					dest: '.tmp/scripts'
					ext: '.js'
				}]

		ngAnnotate:
			options: {
				add: true
			},
			dist:
				files: [
					expand: true
					cwd: ".tmp/concat/scripts",
					src: "*.js"
					dest: ".tmp/concat/scripts"
				]

		copy:
			stuff:
				files: [
					{
						expand: true
						dot: true
						cwd: "<%= config.src %>"
						dest: "<%= config.dist %>"
						src: [
							"index.html"
							"store_items.json"
							"store_items.js"
							"views/{,*/}*.html"
						]
					}
				]
			js:
				files: [
					expand: true
					dot: true
					cwd: ".tmp/scripts"
					dest: "<%= config.dist %>/scripts"
					src: [
						"app.js",
						"scripts.js",
						"vendor.js"
					]
				]
			css:
				files: [
					expand: true
					dot: true
					cwd: ".tmp/styles"
					dest: "<%= config.dist %>/styles"
					src: ["*.css"]
				]

		rev:
			dist:
				files:
					src: [
						"<%= config.dist %>/scripts/{,*/}*.js"
						"<%= config.dist %>/styles/{,*/}*.css"
						"<%= config.dist %>/styles/fonts/*"
					]

		usemin:
			html: ["<%= config.dist %>/{,*/}*.html"]
			css: ["<%= config.dist %>/styles/{,*/}*.css"]
			options:
				assetsDirs: ["<%= config.dist %>"]

		htmlmin:
			options:
				collapseWhitespace: false
				collapseBooleanAttributes: false
				removeCommentsFromCDATA: false
				removeOptionalTags: false

			dist:
				options:
					collapseWhitespace: false
					collapseBooleanAttributes: false
					removeCommentsFromCDATA: false
					removeOptionalTags: false

				files: [
					expand: true
					cwd: "<%= config.dist %>"
					src: [
						"*.html"
						"views/{,*/}*.html"
					]
					dest: "<%= config.dist %>"
				]

	grunt.registerTask "build", [
		"clean:dist"
		"copy:stuff"
		"coffee"
		"useminPrepare"
		"concat"
		"ngAnnotate"
		"cssmin"
		"copy:css"
		"uglify"
		"copy:js"
		"rev"
		"usemin"
		"htmlmin"
	]

	grunt.registerTask "default", [
		"build"
	]
	return