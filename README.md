# README #

**You can use dist.zip contents to quick check last build.**
To disable CORS for Google Chrome use "--disable-web-security" launch flag.

## Environment ##

You need grunt and hs packages globaly installed


```
#!bash

npm -g install hs
npm -g install grunt
```

## Building ##

Go to the project dir and run 

```
#!bash

npm i
bower install
grunt
```

## Launch ##
Go to the project dir and run 
```
#!bash

hs ./dist
```

Have fun.